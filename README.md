# react-all-the-things

> Convert View + template to React component

```js
const View = require('View');
const template = require('templates/test');

const view = View.extend({
    init() {
        this.$el.append(template.render());
    }
});
```

### \+

```html
<!-- templates/test.html -->
<div class="layout">
  <div class="title">
    <p>{title}</p>
  </div>

  <div class="buttons"></div>
</div>
```

### ⚛

```jsx
const View = require('View');

const view = View.extend({
    init() {
        this.$el.append((
            <div className="layout">
              <div className="title">
                <p>{title}</p>
              </div>

              <div className="buttons"></div>
            </div>
        ));
    }
});
```

## Installation
```
npm install -g ratt
```

## Usage
```
$ ratt
Usage: ratt [options] <file>

Options:
  -w, --write           Write result to file instead of printing to stdout
  --templates           Path to templates, default 'platform'
  --externals           Print external references found. Works with .html files only
  -v                    Print version number
```

module.exports = {
    templateRequire: () => [
        { Identifier: /(.*)/g },
        { Punctuator: '=' },
        { Identifier: 'require' },
        { Punctuator: '(' },
        { String: /templates\/(.*)$/g },
        { Punctuator: ')' }
    ]
};

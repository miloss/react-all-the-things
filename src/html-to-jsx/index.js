const fs = require('fs');
const dust2jsx = require('dust2jsx');

const resolveTemplate = require('./resolve-template');

function read(filepath) {
    let content;
    try {
        content = fs.readFileSync(filepath, {
            encoding: 'utf8'
        });
    } catch (e) {
        throw new Error(`Could not load template: ${filepath}`);
    }
    return loadExternalTemplates(content.trim());
}

function padding(line) {
    return ' '.repeat(line.search(/\S|$/));
}

function loadExternalTemplates(html) {
    const lines = html.split('\n');
    return lines.map(line => {
        const trim = line.trim();
        if (trim.startsWith('{>') && trim.endsWith('/}')) {
            const template = loadTemplate(trim);
            const pad = padding(line);
            return template.split('\n').map(line => pad + line).join('\n');
        }
        return line;
    }).join('\n');
}

function loadTemplate(line) {
    const trim = line.replace('{>', '').replace('/}', '').trim();
    const nextQuoteNdx = trim.substr(1).indexOf('"');

    // Passed template parameters via attrs
    const attrParams = trim.substr(nextQuoteNdx + 2).split(' ').reduce((params, assingment) => {
        const attrs = assingment.trim().split('=');
        if (attrs[1] && attrs[1].startsWith('"') && attrs[1].endsWith('"')) {
            params[attrs[0]] = attrs[1].replace(/"/g, '');
        }
        else if (attrs[1]) {
            params[attrs[0]] = `{${attrs[1]}}`;
        }
        return params;
    }, {});

    // Read template file and replace passed params
    let template = read(resolveTemplate(trim.substr(1, nextQuoteNdx)));
    for (let key in attrParams) {
        template = template.replace(new RegExp(`{${key}}`, 'g'), attrParams[key]);
    }

    return template;
}

// Load HTML file content and convert it to JSX
module.exports = (filepath, { context, externals }) => {
    const html = read(filepath);
    return externals ?
        dust2jsx(html, { externals }).join(' ') :
        dust2jsx(html, { context });
};

const fs = require('fs');

// Resolve referenced template path
module.exports = (templatepath, context) => {
    const paths = [
        `${process.env.TEMPLATES_PATH}/${templatepath}.html`,
        `${process.env.TEMPLATES_PATH}/${templatepath}.dust.html`,
        `${process.env.TEMPLATES_PATH}/templates/${templatepath}.dust.html`
    ];

    return paths.find((path) => fs.existsSync(path)) || paths[0];
};

const fs = require('fs');

const htmlToJsx = require('./html-to-jsx');
const templateRenderCall = require('./template-render');

function main(filename, options = {}) {
    if (!filename) {
        console.log("No input specified");
    }

    if (filename.endsWith('.html')) {
        // HTML files - just convert the content
        return htmlToJsx(filename, {
            context: 'props',
            externals: options.externals
        });
    } else {
        // JS files - replace template.render() calls with inline JSX
        return templateRenderCall(fs.readFileSync(filename, {
            encoding: 'utf8'
        }));
    }
}

module.exports = main;

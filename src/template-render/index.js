const esprima = require('esprima');

const config = require('../config');
const match = require('./match');
const replaceRenderCall = require('./replace-render-call');

const LINE_REMOVED = '// --- line removed ---';
const REACT_IMPORTS = [
    `const React = require('react');`,
    `const ReactDOM = require('react-dom');`
];

// Find matching 'require('templates/...')' sequences
function process1(lines) {
    return lines.reduce((templates, line, ndx) => {
        let tokens;
        try {
            tokens = esprima.tokenize(line);
        } catch (e) {
            // Continue if the line parsing fails
            tokens = [];
        }
        if (match.contains(config.templateRequire(), tokens)) {
            templates.push({
                ndx,
                matches: match.getAll(config.templateRequire(), tokens)
            });
        }
        return templates;
    }, []);
}

// Replace 'template.render()' calls with inline HTML
function process2(lines, templateRequires) {
    let found = false;
    templateRequires.forEach((item) => {
        const renderNoParam = `${item.matches[0]}.render()`;
        const renderVarParam = new RegExp(`${item.matches[0]}.render((.*));`);
        const renderObjectParam = `${item.matches[0]}.render({`;

        lines.forEach((line, j) => {
            // .render()
            if (line.indexOf(renderNoParam) > -1) {
                lines.splice(j, 1, ...replaceRenderCall.noParams(line, item.matches));
                lines[item.ndx] = LINE_REMOVED;
                found = true;
            }
            // .render(foo)
            else if (renderVarParam.test(line)) {
                lines.splice(j, 1, ...replaceRenderCall.varParam(line, item.matches));
                lines[item.ndx] = LINE_REMOVED;
                found = true;
            }
            // .render({
            //   foo: 'bar'
            // });
            else if (line.indexOf(renderObjectParam) > -1) {
                const renderLines = [];
                for (let k = j; k < lines.length; k++) {
                    renderLines.push(lines[k]);
                    if (lines[k].endsWith(');')) { break; }
                }
                const newLines = replaceRenderCall.objectParam(renderLines, item.matches);
                lines.splice(j, renderLines.length, ...newLines);
                lines[item.ndx] = LINE_REMOVED;
                found = true;
            }
        });
    });

    // Remove deleted lines
    lines = lines.filter(line => line !== LINE_REMOVED);

    // Do not duplicate 'react' imports if already found
    if (found && lines.indexOf(REACT_IMPORTS[0]) === -1) {
        lines = [...REACT_IMPORTS, ...lines];
    }
    return lines;
}

module.exports = (content) => {
    const lines = content.split('\n');
    const templates = process1(lines);
    return process2(lines, templates).join('\n');
};

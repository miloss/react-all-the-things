
function token(test, token) {
    const key = Object.keys(test)[0];
    if (key !== token.type) {
        return false;
    }

    const val = test[key];
    if (typeof val === 'object') { // Regex
        return val.test(token.value);
    }

    return val === token.value;
}

function sequence(test, tokens) {
    if (test.length !== tokens.length) {
        return false;
    }

    return test.every((item, i) => token(test[i], tokens[i]));
}

function getAll(test, tokens) {
    return test.reduce((matches, item, ndx) => {
        const key = Object.keys(item)[0];
        if (typeof item[key] === 'object') {
            matches.push(tokens[ndx + 1].value);
        }
        return matches;
    }, []);
}

function contains(test, tokens) {
    const diff = tokens.length - test.length;
    if (diff < 0) {
        return false;
    }

    const range = [...Array(diff + 1).keys()];
    return range.some(i => sequence(test, tokens.slice(i, i + test.length)));
}


module.exports = {
    contains,
    getAll
};

const htmlToJsx = require('../html-to-jsx');
const resolveTemplate = require('../html-to-jsx/resolve-template');

const COMPONENT_WRAP = {
    LEFT: `ReactDOM.render(`,
    RIGHT: `, document.createElement('div'))`
};

function loadHtml(match, context) {
    const templatepath = match.substr(1, match.length - 2);
    return htmlToJsx(resolveTemplate(templatepath), { context }).split('\n');
}

function padding(line) {
    return ' '.repeat(line.search(/\S|$/));
}

function mapHtmlLines(pad, line) {
    if (!line.trim()) {
        return '';
    }
    return `${pad}    ${line}`;
}

function noParams(line, matches) {
    const pad = padding(line);
    const call = `${matches[0]}.render()`;
    const ndx = line.indexOf(call);

    return [
        `${line.substr(0, ndx)}${COMPONENT_WRAP.LEFT}(`,
        ...loadHtml(matches[1]).map(mapHtmlLines.bind(null, pad)),
        `${pad})${COMPONENT_WRAP.RIGHT}${line.substr(ndx + call.length)}`
    ];
}

function varParam(line, matches) {
    const pad = padding(line);
    const call = `${matches[0]}.render(`;
    const ndx = line.indexOf(call);

    // Extract single param
    const ndxParen = line.substr(ndx + call.length).indexOf(')');
    const ndxComma = line.substr(ndx + call.length).indexOf(',');
    const param = line.substr(ndx + call.length).substr(0, ndxComma > -1 ? Math.min(ndxParen, ndxComma) : ndxParen);

    // Pad right with existing passed element or default one
    let wrapRight = `${COMPONENT_WRAP.RIGHT}${line.substr(ndx + call.length + ndxParen + 1)}`;
    if (ndxComma > -1) {
        wrapRight = line.substr(ndx + call.length + ndxComma);
    }

    return [
        `${line.substr(0, ndx)}${COMPONENT_WRAP.LEFT}(`,
        ...loadHtml(matches[1], param).map(mapHtmlLines.bind(null, pad)),
        `${pad})${wrapRight}`
    ];
}

function objectParam(lines, matches) {
    if (lines.length === 1) {
        return objectParamOneLine(lines[0], matches);
    }

    const pad = padding(lines[0]);
    const call = `${matches[0]}.render({`;

    return [
        `${pad}const props = {`,
        ...lines.slice(1, lines.length - 1),
        `${pad}};`,
        `${lines[0].replace(call, `${COMPONENT_WRAP.LEFT}(`)}`,
        ...loadHtml(matches[1], 'props').map(mapHtmlLines.bind(null, pad)),
        `${lines[lines.length - 1].replace('})', `)${COMPONENT_WRAP.RIGHT}`)}`
    ];
}

function objectParamOneLine(line, matches) {
    return [
        `const props = { todo };`,
        line
    ];
}

module.exports = {
    noParams,
    varParam,
    objectParam
};

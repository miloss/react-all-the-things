const View = require('View');
const template = require('templates/test-inline-with-params');

const view = View.extend({
    init() {
        this.$el.append(template.render());
    }
});

const React = require('react');
const ReactDOM = require('react-dom');
const View = require('View');

const view = View.extend({
    init() {
        this.$el.append(ReactDOM.render((
            <header></header>

            <div className="content">
              <div className="layout">
                <p className="form__intro">foo</p>

                <div className="icon">
                </div>
              </div>
            </div>
        ), document.createElement('div')));
    }
});

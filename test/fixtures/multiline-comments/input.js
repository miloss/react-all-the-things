const View = require('View');
const template = require('templates/test');

/**
 * Multiline comment that shouldn't brake parsing
 * and should be preserved too
 */
const view = View.extend({
    className: 'view',

    init() {
        View._super.init.apply(this, arguments);

        this.$el.append(template.render());
    }
});

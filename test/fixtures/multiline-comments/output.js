const React = require('react');
const ReactDOM = require('react-dom');
const View = require('View');

/**
 * Multiline comment that shouldn't brake parsing
 * and should be preserved too
 */
const view = View.extend({
    className: 'view',

    init() {
        View._super.init.apply(this, arguments);

        this.$el.append(ReactDOM.render((
            <div className="layout">
              <p className="form__intro">{intro}</p>

              <div className="icon">
              </div>
            </div>
        ), document.createElement('div')));
    }
});

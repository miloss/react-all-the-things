const View = require('View');
const small = require('templates/test-small');
const template = require('templates/test');

const foo = {};

const view = View.extend({
    className: 'view',

    init() {
        View._super.init.apply(this, arguments);

        small.render(foo, this.$el);

        this.$el.append(template.render(foo));
    }
});

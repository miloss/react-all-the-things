const View = require('View');
const template = require('templates/test');

const view = View.extend({
    className: 'view',

    init() {
        const foo = 'bar';
        template.render({ foo }, this.$el)
    }
});

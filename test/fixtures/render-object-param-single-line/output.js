const React = require('react');
const ReactDOM = require('react-dom');
const View = require('View');

const view = View.extend({
    className: 'view',

    init() {
        const foo = 'bar';
        const props = { foo };
        this.$el.append(ReactDOM.render((
            <div className="layout">
              <p className="form__intro">{props.intro}</p>

              <div className="icon">
              </div>
            </div>
        ), this.$el));
    }
});

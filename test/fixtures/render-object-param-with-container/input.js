const View = require('View');
const template = require('templates/test');

const view = View.extend({
    className: 'view',

    init() {
        this.$el.append(template.render({
            foo: 'bar'
        }, this.container, 42));
    }
});

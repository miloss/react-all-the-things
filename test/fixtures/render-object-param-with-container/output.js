const React = require('react');
const ReactDOM = require('react-dom');
const View = require('View');

const view = View.extend({
    className: 'view',

    init() {
        const props = {
            foo: 'bar'
        };
        this.$el.append(ReactDOM.render((
            <div className="layout">
              <p className="form__intro">{props.intro}</p>

              <div className="icon">
              </div>
            </div>
        ), this.container, 42));
    }
});

const React = require('react');
const ReactDOM = require('react-dom');
const View = require('View');

const view = View.extend({
    className: 'view',

    init() {
        View._super.init.apply(this, arguments);

        const props = {
            // Comment should be preserved
            foo: 'bar'
        };
        this.$el.append(ReactDOM.render((
            <div className="layout">
              <p className="form__intro">{props.intro}</p>

              <div className="icon">
              </div>
            </div>
        ), document.createElement('div')));
    }
});

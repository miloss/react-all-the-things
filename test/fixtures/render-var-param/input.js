const View = require('View');
const template = require('templates/test');

const foo = {};

const view = View.extend({
    className: 'view',

    init() {
        View._super.init.apply(this, arguments);

        this.$el.append(template.render(foo));
    }
});

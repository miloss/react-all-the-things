const React = require('react');
const ReactDOM = require('react-dom');
const View = require('View');

const foo = {};

const view = View.extend({
    className: 'view',

    init() {
        View._super.init.apply(this, arguments);

        this.$el.append(ReactDOM.render((
            <HeaderComponent logo={foo}/>

            <div className="layout">
              <p className="form__intro">{foo.intro}</p>

              {foo.condition ?
                <span>{foo.text}</span>
               : null}

              <div className="text">
                <p>{foo.bar.baz}</p>
              </div>

              <FooterComponent />
            </div>
        ), document.createElement('div')));
    }
});

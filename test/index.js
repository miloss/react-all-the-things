const chai = require('chai');
const chaiFiles = require('chai-files');

chai.use(chaiFiles);

const expect = chai.expect;
const file = chaiFiles.file;

const ratt = require('../src');

process.env.TEMPLATES_PATH = __dirname;

describe('react-all-the-things', () => {

    it('should replace template.render() call', () => {
        const result = ratt('test/fixtures/render/input.js');
        expect(result).to.equal(file('test/fixtures/render/output.js'));
    });

    it('should replace template.render() call with variable parameter', () => {
        const result = ratt('test/fixtures/render-var-param/input.js');
        expect(result).to.equal(file('test/fixtures/render-var-param/output.js'));
    });

    it('should replace template.render() call with object parameter', () => {
        const result = ratt('test/fixtures/render-object-param/input.js');
        expect(result).to.equal(file('test/fixtures/render-object-param/output.js'));
    });

    it('should preserve multi-line comments', () => {
        const result = ratt('test/fixtures/multiline-comments/input.js');
        expect(result).to.equal(file('test/fixtures/multiline-comments/output.js'));
    });

    it('should replace template variables with props', () => {
        const result = ratt('test/fixtures/replace-props/input.js');
        expect(result).to.equal(file('test/fixtures/replace-props/output.js'));
    });

    it('should include referenced external templates', () => {
        const result = ratt('test/fixtures/inline-partials/input.js');
        expect(result).to.equal(file('test/fixtures/inline-partials/output.js'));
    });

    it('should include referenced external templates with passed parameters', () => {
        const result = ratt('test/fixtures/inline-partials-with-params/input.js');
        expect(result).to.equal(file('test/fixtures/inline-partials-with-params/output.js'));
    });

    it('should keep render context and render all referenced templates', () => {
        const result = ratt('test/fixtures/multiple-render-context/input.js');
        expect(result).to.equal(file('test/fixtures/multiple-render-context/output.js'));
    });

    it('should convert content if input is HTML file', () => {
        const result = ratt('test/templates/test-inline.html');
        expect(result + '\n').to.equal(file('test/templates/test-inline.jsx'));
    });

    it('should return external references', () => {
        const result = ratt('test/templates/test-props.html', { externals: true });
        expect(result).to.equal('HeaderComponent FooterComponent');
    });
});
